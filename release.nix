# -*- coding: utf-8 -*-
# :Project:   SoL -- Nix derivation
# :Created:   sab 04 ago 2018 22:57:25 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
# :Copyright: © 2020, 2021, 2024 Lele Gaifax
#

{
  pkgs ? import <nixpkgs> { },
  py ? pkgs.python3,
  gitignoreFilterWith ? (
    let
      gitignoreSrc = pkgs.fetchFromGitHub {
        owner = "hercules-ci";
        repo = "gitignore.nix";
        rev = "637db329424fd7e46cf4185293b9cc8c88c95394";
        hash = "sha256-HG2cCnktfHsKV0s4XW83gU3F57gaTljL9KNSuG6bnQs=";
      };
      gitignore = import gitignoreSrc { inherit (pkgs) lib; };
    in
    gitignore.gitignoreFilterWith
  ),
}:

let
  inherit (builtins) fromTOML readFile;
  pypkgs = py.pkgs;
  deps = import ./dependencies.nix { inherit pkgs pypkgs; };
  pinfo = (fromTOML (readFile ./pyproject.toml)).project;
  getSource =
    name: path:
    pkgs.lib.cleanSourceWith {
      name = name;
      src = path;
      filter = gitignoreFilterWith { basePath = path; };
    };
  # See https://github.com/pytest-dev/pytest/issues/12123
  pytest802 = pypkgs.pytest.overridePythonAttrs (old: rec {
    pname = "pytest";
    version = "8.0.2";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-1AUdYjouC35RlgupYxk7Cc5trrl1mkUYRKIeTd7fwb0=";
    };
  });
in
pypkgs.buildPythonPackage rec {
  pname = pinfo.name;
  version = pinfo.version;
  src = getSource "SoL" ./.;
  pyproject = true;
  nativeBuildInputs = [
    pkgs.gnumake
    pkgs.which
  ];
  build-system = [
    deps.metapensiero-extjs-desktop

    pypkgs.pdm-backend
    pypkgs.babel
    pypkgs.sphinx
  ] ++ deps.metapensiero-extjs-desktop.optional-dependencies.dev;
  dependencies = [
    deps.metapensiero-extjs-desktop
    deps.metapensiero-sqlalchemy-proxy
    deps.pyramid-mailer
    deps.pyramid-tm
    deps.repoze-sendmail
    deps.zope-sqlalchemy

    pypkgs.alembic
    pypkgs.babel
    pypkgs.itsdangerous
    pypkgs.mako
    pypkgs.markupsafe
    pypkgs.pycountry
    pypkgs.pynacl
    pypkgs.pyramid
    pypkgs.pyramid-mako
    pypkgs.python-rapidjson
    pypkgs.reportlab
    pypkgs.ruamel-yaml
    pypkgs.setuptools # for pkg_resources, used by Pyramid
    pypkgs.sqlalchemy
    pypkgs.transaction
    pypkgs.translationstring
    pypkgs.waitress
    pypkgs.xlsxwriter
  ];
  optional-dependencies = {
    dev = [
      deps.bump-my-version
      deps.metapensiero-sqlalchemy-dbloady

      pypkgs.build
      pypkgs.calmjs-parse
      pypkgs.docutils
      pypkgs.hupper
      pypkgs.pygments
      pypkgs.requests
      pkgs.ruff
      pypkgs.sphinx
      pypkgs.twine
    ] ++ deps.metapensiero-extjs-desktop.optional-dependencies.dev;
    test = [
      pypkgs.coverage
      pytest802
      pypkgs.pytest-cov
      pypkgs.webtest
    ];
  };
  doCheck = false;
  medsrc =
    deps.metapensiero-extjs-desktop + "/lib/${py.libPrefix}/site-packages/metapensiero/extjs/desktop";
  preBuild = ''
    substituteAllInPlace Makefile.release
    make IN_NIX_SHELL=1 MEDSRC=${medsrc} compile-catalogs manual minimize
  '';
}
