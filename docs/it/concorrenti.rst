.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 04 feb 2014 09:13:17 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014, 2020, 2024 Lele Gaifax
..

.. _correzione concorrenti:

Correzione concorrenti
~~~~~~~~~~~~~~~~~~~~~~

.. index::
   pair: Correzione concorrenti; Tornei

È già successo, accadrà di nuovo: qualcuno ha comunicato il nome sbagliato, o è stato frainteso
o quant'altro, fatto sta che uno degli iscritti a un torneo è la persona sbagliata e bisogna
correggere manualmente la lista dei concorrenti.

.. important:: Il problema può essere corretto in qualsiasi momento, anche dopo aver effettuato
               la premiazione. La modifica però dovrebbe essere fatta **prima** di diffondere i
               dati del torneo ad altre istanze di SoL, altrimenti la medesima correzione dovrà
               essere rieffettuata manualmente su ciascuna di esse.

Con il pulsante :guilabel:`Concorrenti` della finestra :ref:`gestione tornei` si ottiene la
solita griglia di gestione dei concorrenti di un particolare torneo. Non è possibile inserire o
togliere giocatori (usa le funzionalità del :ref:`pannello concorrenti` per quello), ma si può
sostituire qualsiasi giocatore con qualsiasi altro.

.. warning:: Dal momento che la maschera effettua pochissimi controlli, è necessario usare
             cautela controllando due volte prima di confermare le modifiche. Ad esempio, i
             combo consentono di inserire lo *stesso* giocatore due volte, cosa che non sarà
             accettata dal database e causerà quindi un errore.

.. _posizione concorrenti:

Posizione
---------

Alcuni :ref:`tipi di torneo <sistema torneo>`, in particolare quando usati con il :ref:`metodo
di abbinamento <abbinamenti>` ``teste di serie``, richiedono che i concorrenti siano ordinati
in base alla loro forza, di solito indicata dalla loro *valutazione*.

Nei casi in cui il torneo non sia associato ad una :ref:`valutazione <gestione valutazioni
glicko>`, oppure quando più di un concorrente ha lo stesso indice, è necessario assegnare
manualmente la :guilabel:`posizione` esplicita: i valori sono *arbitrari* e sono relativi, non
assoluti: i concorrenti verranno ordinati secondo questo valore, in ordine crescente.

La voce :guilabel:`Assegna posizioni` nel menu automaticamente riassegna un numero
incrementale, a partire da 1 (uno), a tutti i concorrenti nell'esatto ordine visibile nella
tabella.

Se inserite, le posizioni hanno priorità sulla *valutazione* dei concorrenti.
