.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mer 09 lug 2014 10:11:06 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

======================================
 :mod:`sol.models.bio` -- Batched I/O
======================================

.. automodule:: sol.models.bio

.. autofunction:: save_changes

.. autofunction:: backup
.. autofunction:: restore

.. autofunction:: load_sol
.. autofunction:: dump_sol

.. autoclass:: Serializer
   :members:

.. autoclass:: Deserializer
   :members:
