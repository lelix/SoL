((nil . ((esk/project-name . "SoL")
         (esk/project-license . "GNU General Public License version 3 or later")))
 (python-mode . ((pycov-coverage-file . "coverage.json"))))
