.. -*- coding: utf-8 -*-

Changes
-------

5.4 (2024-11-27)
~~~~~~~~~~~~~~~~

* Fix "home page" URL in the Lit pages (issue `#45`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/44

* Disable the refresh action on the Matches panel, when it is showing a previous round (issue
  `#44`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/44

* Implement a way to move a match onto a different board (issue `#43`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/43


5.3 (2024-11-06)
~~~~~~~~~~~~~~~~

* Add player's nationality to the rating ranking printout, when there are multiple nationalities


5.2 (2024-10-18)
~~~~~~~~~~~~~~~~

* Disallow refreshing the matches panel in prized tournaments


5.1 (2024-08-21)
~~~~~~~~~~~~~~~~

* Add respective current opponent's info in the ranking table, to make it easier reasoning
  about pairing algorithms and options (issue `#42`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/42

* Disallow removing competitors in a readonly tournament

* Simplify adding new competitor to singles events


5.0 (2024-08-12)
~~~~~~~~~~~~~~~~

* No visible changes, mark 5.0 as final


5.0.rc2 (2024-07-07)
~~~~~~~~~~~~~~~~~~~~

* Fix rendering of *delaycompatriotpairing* in the tourney Lit details

* Fix rendering of knockout diagram when the competitors rate is unknown

* Prevent double submission of changes in the digital scorecards


5.0.rc1 (2024-07-04)
~~~~~~~~~~~~~~~~~~~~

* Fix not-so-common case in re-upload of tournament data, a regression introduced in 5.0.dev3


5.0.rc0 (2024-07-03)
~~~~~~~~~~~~~~~~~~~~

* No visible changes, just to start the rc phase


5.0.dev6 (2024-07-02)
~~~~~~~~~~~~~~~~~~~~~

* Fix a recent regression: give back full power to change players and clubs to the admin user


5.0.dev5 (2024-05-30)
~~~~~~~~~~~~~~~~~~~~~

* Remove automatic reload of the digital scorecard, wrong in any aspect


5.0.dev4 (2024-05-29)
~~~~~~~~~~~~~~~~~~~~~

* Improve the logic used to assign carromboards to matches

* Rectify visualization of ranking, when focused on the current round

* Use HTML5 instead of obsolete XHTML in most pages


5.0.dev3 (2024-05-23)
~~~~~~~~~~~~~~~~~~~~~

* Rectify URL embedded in the mail sent to new user when she confirms the account

* Users may now be elected as *responsable* of a particular country, allowing them to modify
  clubs and players of that country, even when owned by other users


5.0.dev2 (2024-05-17)
~~~~~~~~~~~~~~~~~~~~~

* Fix brown paper bag bug introduced in the "all" coupling variant

* Tweak ``trainingboards`` label and hint (issue `#23`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/23

* Take into account the seeds/rates in the knockout ranking

* Add action to the ``Competitors`` manage window to automatically assign positions


5.0.dev1 (2024-05-14)
~~~~~~~~~~~~~~~~~~~~~

* Make players and clubs nationality mandatory

* Allow more than one tournament on the same day in the same championship

* Replace the pairing method *all against all* with a more correct *Round-robin* tourney
  system: the ``all`` couplings variant remains, because in some rare cases it's needed to
  generate all turns, albeit "incomplete"

* Strengthen the logic used to discern *named phantoms* (issue `#38`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/38

* Handle *best of three games* matches in *knockout* tourneys (issue `#39`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/39

* Tweak the generation of the *next turn* in knockout tourneys, to better mimic the `standard
  seeding`__ method (issue `#40`__)

  __ https://en.wikipedia.org/wiki/Single-elimination_tournament#Seeding
  __ https://gitlab.com/metapensiero/SoL/-/issues/40

* Show the usual `diagram`__ on knockout tourneys Lit page

  __ https://en.wikipedia.org/wiki/Single-elimination_tournament#Example

* Get rid of the fantasy pairing method ``extremes`` in knockout tourneys

* Remove unmaintained translations, most of them were seriously out of date

* Remove unused and incomplete Docker stuff

* Various enhancements to the digital scorecard:

  - when the player tries to start the game before the countdown is started
    she is gently asked to wait until that happens

  - when the Queen gets pocketed, the server is immediately informed so that people watching
    the Lit page of the match will see it

  - the automatic dimming of the device's display can be disabled and re-enabled at will, to
    reduce distracting gestures to resume the session when entering scores

* Users now have an upper limit on the level of ratings they can use/create: the rationale is
  that only an handful of them are expected to run international events, and all the other
  should use local or national ratings

* Rectify and improve handling of the “delay compatriots pairing”: it is now a number of
  rounds, similar to the “delay top players pairing”, instead of a boolean flag (issue `#41`__)

  __ https://gitlab.com/metapensiero/SoL/-/issues/41
