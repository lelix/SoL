# -*- coding: utf-8 -*-
# :Project:   SoL -- Main makefile
# :Created:   sab 08 nov 2008 21:16:39 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2008-2010, 2013-2016, 2018-2024 Lele Gaifax
#

include Makefile.defs

.PHONY: clean
clean::
	$(MAKE) -C docs clean
	rm -rf build $(SOLSRC)/static/manual
	rm -f $(PACKED_CSS) $(PACKED_JS)
	-@find docs src -name '*~' -print0 | xargs -r0 rm

.PHONY: distclean
distclean:: clean
	rm -f development.db

development.db:
	$(SOLADMIN) initialize-db development.ini
	@read -sp "Enter secret key to load from production, or nothing to load test data: " secretkey && \
	  if [[ $$secretkey && $${#secretkey} == 64 ]]; then \
	    echo -e "\nLoading from production..."; \
	    echo $(SOLADMIN) restore --secret-key "$$secretkey" development.ini; \
	    $(SOLADMIN) restore --secret-key "$$secretkey" development.ini; \
	  else \
	    echo -e "\nLoading test data..."; \
	    $(DBLOADY) tests/data.dbloady; \
	  fi

.PHONY: generate-db-migration
generate-db-migration:
	read -p "Enter migration description: " MDESC && \
	  $(ALEMBIC) revision --autogenerate -m "$$MDESC"

.PHONY: upgrade-db
upgrade-db:
	$(SOLADMIN) upgrade-db development.ini

.PHONY: serve
serve: development.db compile-catalogs
	pserve --reload development.ini

$(COVERAGE_DATA):
	-@$(COVERAGE) combine --quiet

$(COVERAGE_JSON): $(COVERAGE_DATA)
	-@$(COVERAGE) json --quiet -o $@

.PHONY: test
test: compile-catalogs
	@$(PYTEST)
	@$(MAKE) $(COVERAGE_JSON)

.PHONY: test-slow
test-slow: compile-catalogs
	@$(PYTEST) --runslow
	@$(MAKE) $(COVERAGE_JSON)

.PHONY: test-fail-soon
test-fail-soon: compile-catalogs
	@$(PYTEST) -x

.PHONY: test-format
test-format: ruff-check-imports ruff-check-format

.PHONY: ruff-check-imports
ruff-check-imports:
	$(RUFF) check --select I src/

.PHONY: ruff-fix-imports
ruff-fix-imports:
	$(RUFF) check --select I --fix src/

.PHONY: ruff-check-format
ruff-check-format:
	$(RUFF) format --check src/ tests/

.PHONY: ruff-fix-format
ruff-fix-format:
	$(RUFF) format src/ tests/

include Makefile.i18n
include Makefile.manual
include Makefile.release
