# -*- coding: utf-8 -*-
# :Project:   SoL -- Nix targets
# :Created:   sab 21 set 2019 00:01:21 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
# :Copyright: © 2021, 2022, 2024 Lele Gaifax
#

RUNVM_TOKEN := .runvm-created
NREBUILD := nixos-rebuild

$(RUNVM_TOKEN): $(PWD)/dependencies.nix
$(RUNVM_TOKEN): $(PWD)/release.nix
$(RUNVM_TOKEN): $(PWD)/nixos/config.nix
$(RUNVM_TOKEN): $(PWD)/nixos/sol-site-activation.sh
$(RUNVM_TOKEN): $(PWD)/nixos/vmtest.nix
	NIXOS_CONFIG=$< ${NREBUILD} --no-flake build-vm
	touch $(@)

.PHONY: run-vm
run-vm: $(RUNVM_TOKEN)
	QEMU_NET_OPTS="hostfwd=tcp:0.0.0.0:6996-:6996" result/bin/run-vmhost-vm

.PHONY: clean
clean::
	rm -f $(RUNVM_TOKEN)
	rm -rf result*
	rm -f vmhost.qcow2

.PHONY: nix-release
nix-release:
	nix-build release.nix
