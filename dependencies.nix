# -*- coding: utf-8 -*-
# :Project:   SoL -- Derivations for some non packaged dependencies
# :Created:   sab 04 ago 2018 22:57:25 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
# :Copyright: © 2020-2024 Lele Gaifax
#

{
  pkgs ? import <nixpkgs> { },
  pypkgs ? pkgs.python3Packages,
}:
rec {

  bump-my-version = pypkgs.buildPythonPackage rec {
    pname = "bump-my-version";
    version = "0.28.1";
    src = pypkgs.fetchPypi {
      pname = "bump_my_version";
      inherit version;
      hash = "sha256-5gje9Rkbr1BbbN6IvWeaCpX8TP6s5CR622CsD4p+V+4=";
    };
    pyproject = true;
    build-system = [ pypkgs.hatchling ];
    dependencies = [
      pypkgs.click
      pypkgs.pydantic
      pypkgs.pydantic-settings
      pypkgs.questionary
      pypkgs.rich
      pypkgs.rich-click
      pypkgs.tomlkit
      pypkgs.wcmatch
    ];
  };

  metapensiero-extjs-desktop =
    let
      extjs = pkgs.fetchzip {
        url = "https://cdn.sencha.com/ext/gpl/ext-4.2.1-gpl.zip";
        hash = "sha256-bb194GkgKVfvFt3VjcqhQ6qAht+hNxDd7MDTS2j26VI=";
      };
    in
    pypkgs.buildPythonPackage rec {
      inherit extjs;
      pname = "metapensiero.extjs.desktop";
      version = "2.4";
      src = pypkgs.fetchPypi {
        inherit pname version;
        hash = "sha256-2XYTI72GtXAs5hhBR6FUkGPjAGH785WpOuzSVpjIWvg=";
      };
      doCheck = false;
      build-system = [ pypkgs.setuptools ];
      preBuild = ''
        mkdir -p src/metapensiero/extjs/desktop/assets/extjs
        cp -a $extjs/resources $extjs/src $extjs/ext-dev.js src/metapensiero/extjs/desktop/assets/extjs
        substituteInPlace MANIFEST.in --replace "prune src/metapensiero/extjs/desktop/assets/extjs" "recursive-include src/metapensiero/extjs/desktop/assets/extjs *.gif *.png *.jpg *.js *.css"
        substituteAllInPlace src/metapensiero/extjs/desktop/scripts/minifier.py
      '';
      optional-dependencies = {
        dev = [
          pypkgs.babel
          pypkgs.calmjs-parse
          pypkgs.pyramid
          pypkgs.rcssmin
          pypkgs.rjsmin
        ];
      };
    };

  metapensiero-sqlalchemy-dbloady = pypkgs.buildPythonPackage rec {
    pname = "metapensiero.sqlalchemy.dbloady";
    version = "3.0.dev3";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-n6j+EkN5uITLr4d921L409A7xkCUZmM7l4JFO0UvRFw=";
    };
    pyproject = true;
    doCheck = false;
    build-system = [ pypkgs.pdm-pep517 ];
    dependencies = [
      pypkgs.progressbar2
      pypkgs.ruamel-yaml
      pypkgs.sqlalchemy
    ];
  };

  metapensiero-sqlalchemy-proxy = pypkgs.buildPythonPackage rec {
    pname = "metapensiero_sqlalchemy_proxy";
    version = "6.0.dev9";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-8nuGnZIL9DKM3+XZ9X6qCvVhSUXiUG+vONfocI4ueUQ=";
    };
    pyproject = true;
    doCheck = false;
    build-system = [ pypkgs.pdm-backend ];
    propagatedBuildInputs = [ pypkgs.sqlalchemy ];
  };

  pyramid-mailer = pypkgs.buildPythonPackage rec {
    pname = "pyramid_mailer";
    version = "0.15.1";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-7Ar/VNkXmyqiki/4LCAWpNyNHaXcNAjWWU8OIJZEb5s=";
    };
    doCheck = false;
    build-system = [ pypkgs.setuptools ];
    dependencies = [
      pypkgs.pyramid
      pypkgs.transaction
      repoze-sendmail
    ];
  };

  pyramid-tm = pypkgs.buildPythonPackage rec {
    pname = "pyramid_tm";
    version = "2.5";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-XIHc7NM3cPXjWWaH0r41/8T4zl7aAKMaywCuNaUUMNA=";
    };
    doCheck = false;
    build-system = [ pypkgs.setuptools ];
    dependencies = [
      pypkgs.pyramid
      pypkgs.transaction
    ];
  };

  repoze-sendmail = pypkgs.buildPythonPackage rec {
    pname = "repoze.sendmail";
    version = "4.4.1";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-eo6jeRSl04utOAUqg+rB2Gexcf9MyLTUmU6JLAWw1CQ=";
    };
    doCheck = false;
    build-system = [ pypkgs.setuptools ];
    dependencies = [
      pypkgs.transaction
      pypkgs.zope_interface
    ];
  };

  zope-sqlalchemy = pypkgs.buildPythonPackage rec {
    pname = "zope.sqlalchemy";
    version = "3.1";
    src = pypkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-2cLDvmlcITxeIrf3xqSiFPqOtZQLAzRluhwQqdizRts=";
    };
    doCheck = false;
    build-system = [ pypkgs.setuptools ];
    dependencies = [
      pypkgs.packaging
      pypkgs.sqlalchemy
      pypkgs.transaction
      pypkgs.zope-interface
    ];
  };
}
